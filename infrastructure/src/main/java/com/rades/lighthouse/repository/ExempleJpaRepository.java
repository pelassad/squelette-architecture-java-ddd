package com.rades.lighthouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExempleJpaRepository extends JpaRepository {
}
